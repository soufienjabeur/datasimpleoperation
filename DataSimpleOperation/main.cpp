#include <fstream>
#include <fstream>
#include <string>
#include<cstdlib>
#include <algorithm>
#include <cmath>
#include <iostream>

using namespace std;
    int N=200;
    int Data[200];
    int median_Data[200];
     int t_mod[10];
     int t_eff[10];
     int t_eff_cumule[10];
     int t_quartile[3];
     int t_decile[9];
     double t_freq[10];
     double t_freq_cumule[10];
     int k;
     int r;
  
  void generateData(/*int N,*/int Max, int Min/*,int *Data*/ )
{
    int i;
    int f=Max-Min;
    for (i=0;i<N;i++)
    Data[i]=  rand()%f+Min; 
     
}   
     void write(char * fich/*, int * Data, int S*/)
{
    int i;
    FILE *fr;
    fr=fopen(fich,"w");
    for (i=0;i<N;i++)
    
    fprintf (fr,"%d ",Data[i]) ;

    fclose(fr);

}

void read(char * fich)
{
    int i;
    FILE *fr;
    fr=fopen(fich,"r");
    for (i=0;i<N;i++)
   
    fscanf (fr, " %d ", &Data[i]) ;

    fclose(fr);

}

void showData ( )
{   int i;
    printf("Data: \n");
    
    for (i=0;i<N;i++)
       printf(" %d  ", Data[i]);
    printf("\n ");
}


void count_eff()
{

    int i,j;
    t_mod[0]=Data[0];
    t_eff[0]=1;
    k=1;

    for(i=1;i<N;i++)
	{     j=0;
	      while (j<k)
	      { 
		       if (t_mod[j]==Data[i])
		       {
			         t_eff[j]=t_eff[j]+1;
			        j=k+1;
		       }
		       else j++;
	      }
	      if (j==k)
	      { 
		      t_mod[j]=Data[i];
		      t_eff[j]=1;
		      k++;
	      }
    
    }
    
}
void count_freq()
{
    int i;
    float y;
    
    for(i=0;i<k;i++)
    
      t_freq[i]=(float)t_eff[i]/(float)N;
      
}
void effectif_cumule()
{
	int i;
	int j;
	int somme;
	for(i=0;i<10;i++)
	{
		somme=0;
		for(j=0;j<=i;j++)
		{
			somme=somme+t_eff[j];
			
		}
		t_eff_cumule[i]=somme;
		
	}
}
void frequence_cumule()
{
	int i;
	int j;
	double somme;
	for(i=0;i<10;i++)
	{
		somme=0;
		for(j=0;j<=i;j++)
		{
			somme=somme+t_freq[j];
			
		}
		t_freq_cumule[i]=somme;
		
	}
}
double mode()
{
	double max=t_freq[0];
	int i;
	for(i=1;i<k;i++)
	{
		if(t_freq[i]>max)
		{
			max=t_freq[i];
		}
	}
}
int median()
{
	int i;
	int x;
	for(i=0;i<N;i++)
	{
		median_Data[i]=Data[i];
	}
	std::sort(median_Data,median_Data+N);
	x=(N/2)+1;
	return median_Data[x];
}

int moyenne()
{
	double moyenne=0;
	int i;
	int aux=0;
	for(i=0;i<k;i++)
	{
		aux=aux+(t_mod[i]*t_eff[i]);
	}
	moyenne=aux/N;
	return moyenne;
}
int etendu()
{
	int i;
	int etendu;
	for(i=0;i<N;i++)
	{
		median_Data[i]=Data[i];
	}
	std::sort(median_Data,median_Data+N);
	etendu=median_Data[N-1]-median_Data[0];
	return etendu;
	
}

void quartile()
{
	int i;
	for(i=0;i<N;i++)
	{
		median_Data[i]=Data[i];
	}
	std::sort(median_Data,median_Data+N);
	for(i=1;i<=3;i++)
	{
		
		t_quartile[i]=median_Data[(i*N)/4];	
	}
}
void decile()
{
	int i;
	for(i=0;i<N;i++)
	{
		median_Data[i]=Data[i];
	}
	std::sort(median_Data,median_Data+N);
	for(i=1;i<=9;i++)
	{
		
		t_decile[i]=median_Data[(i*N)/10];	
	}
}

int Ecart_absolu_moyen()
{
	int moyen=moyenne();
	int ecart_absolu_moyenne=0;
	int i;
	int aux=0;
	for(i=0;i<k;i++)
	{
		aux=aux+(t_eff[i]*(std::abs(t_mod[i]-moyen)));
	}
	ecart_absolu_moyenne=aux/N;
	return ecart_absolu_moyenne;
}

int Ecart_absolu_median()
{
	int med=median();
	int ecart_absolu_median=0;
	int i;
	int aux=0;
	for(i=0;i<k;i++)
	{
		aux=aux+(t_eff[i]*(std::abs(t_mod[i]-med)));
	}
	ecart_absolu_median=aux/N;
	return ecart_absolu_median;
}

int variance()
{
	int moyen=moyenne();
	int var=0;
	int i;
	int aux=0;
	int x=0;
	int z=0;
	for(i=0;i<k;i++)
	{
		x=t_eff[i]*t_mod[i];
		z=x-moyen;
		aux=aux+ std::pow(z,2);
	}
	var=aux/N;
	return var;
}

int ecart_type()
{
	return std::sqrt(variance());
}

int moment(int r)
{
	int aux=0;
	int i;
	if(r==0)
	{
		return 1;
	}
	else if(r==1)
	{
		return moyenne();
	 } 
	 else
	 {
	 	for(i=0;i<k;i++)
	 	{
	 		aux=aux+std::pow((t_eff[i]*t_mod[i]),r);
		 }
		 return aux/N;
	 }
}
int moment_centre(int r)
{
	int aux=0;
	int i;
	int moy=moyenne();
	int x;
	int z;
	if(r==0)
	{
		return 1;
	}
	else if(r==1)
	{
		return 0;
	 } 
	 else if(r==2)
	 {
	 	return variance();
	 }
	 else
	 {
	 	for(i=0;i<k;i++)
	 	{
	 		x=t_eff[i]*t_mod[i];
			z=x-moy;
	 		aux=aux+std::pow(z,r);
		 }
		 return aux/N;
	 }
}

void showData1 ( )
{   int i;
     float y=0.0;
     printf("\n ------------------------------------------------------------------------------------------------------------\n");
    printf("\n Mod�larit�   ");
    for (i=0;i<k;i++)
     
      printf("%d      ", t_mod[i]);
      printf("\n");
      printf("\n Effectif     ");
    for (i=0;i<k;i++)
     
      printf("%d      ", t_eff[i]);
      printf("\n");
      
      printf("\n Fr�quence    ");
    for (i=0;i<k;i++)
     
     
      printf("%2.4f  ", t_freq[i]);
    

    printf("\n");
   printf("\n Effectif cumul�    ");
    for (i=0;i<k;i++)
     
      printf("%d      ", t_eff_cumule[i]);
      printf("\n");
      
      printf("\n");
   printf("\n Fr�quence  cumul�e    ");
    for (i=0;i<k;i++)
     
      printf("%2.4f      ", t_freq_cumule[i]);
      printf("\n");
      
         printf("\n");
   printf("\n le Mode :   ");
    printf("%2.4f      ", mode());
      printf("\n"); 
      
	  printf("\n le M�diane :   ");
    printf("%d      ", median());
      printf("\n"); 
	  
	  printf("\n la moyenne :   ");
    printf("%d      ", moyenne());
      printf("\n");  
      
       printf("\n l'etendu:   ");
    printf("%d      ", etendu());
      printf("\n");
	  
	  printf("\n Les quantiles:    ");
    for (i=1;i<=3;i++)
     
      printf("%d      ", t_quartile[i]);
      printf("\n"); 
	  
	  printf("\n Les d�ciles:    ");
    for (i=1;i<=9;i++)
     
      printf("%d      ", t_decile[i]);
      printf("\n");  
	  
	  
      printf("\n Ecart Inter-Quartiles:    ");
      printf("%d      ", t_quartile[3]-t_quartile[1]);
      printf("\n"); 
	  
	  printf("\n Ecart Inter-D�ciles:    ");
      printf("%d      ", t_decile[9]-t_decile[1]);
      printf("\n"); 
	  
	  printf("\n l'ecart absolu moyen:   ");
    printf("%d      ", Ecart_absolu_moyen());
      printf("\n");
	  
	  printf("\n l'ecart absolu m�dian:   ");
    printf("%d      ", Ecart_absolu_median());
      printf("\n"); 
	  
	   printf("\n La variance:   ");
    printf("%d      ", variance());
      printf("\n"); 
	  
	   printf("\n L'�cart type':   ");
    printf("%d      ", ecart_type());
      printf("\n");
	  
	   printf("\n*** Le moment ***  ");
	  printf("\n Veuillez saisir l'orde':   ");
	  cin>>r;
	  printf("\n Le moment d'ordre %d :   ",r);
    printf("%d      ", moment(r));
      printf("\n"); 
      
      printf("\n*** Le moment centr� ***  ");
	  printf("\n Veuillez saisir l'orde':   ");
	  cin>>r;
	  printf("\n Le moment centr� d'ordre %d :   ",r);
    printf("%d      ", moment_centre(r));
      printf("\n"); 
}
int main() 
{ 
 
 int Max=2;
 int Min=12;
 char f[20]= "travail.txt";

 
 generateData(Max,Min);
 write(f);
 read(f); 
 showData();
 count_eff();
 count_freq();
 effectif_cumule();
 frequence_cumule();
 quartile();
 decile();
 showData1();
   
 return 0;
}
